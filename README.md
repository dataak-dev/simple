# Simple

## Quick Installation in local

```bash
cp .env.example .env
cp .env.production.example .env.production

echo WWWUSER=${WWWUSER:-$UID} >> .env
echo WWWGROUP=${WWWGROUP:-$(id -g)} >> .env
```
- then
```bash

docker-compose run --rm develop.app bash

composer install

npm install

./vendor/bin/rr get-binary --filter=2.7.9

chmod +x ./rr

php artisan key:generate --ansi
```

- exit.

```bash
docker compose up develop.app
docker compose exec develop.app bash

```
- [http://localhost:8000/](http://localhost:8000/)
- enjoy!


## Deployment
- copy `.env.production.example` content to `.env.production` in server, then change values

```bash
APP_KEY=base64:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=
```

docker
```bash
docker login registry.gitlab.com

docker run -p 80:80 --env-file=.env.production --network=db registry.gitlab.com/dataak-dev/simple:latest
```
docker-compose
```yaml
version: '3'
services:
    production.app:
        image: registry.gitlab.com/dataak-dev/simple:latest
        ports:
            - '${APP_PORT:-80}:80'
        env_file:
            - .env.production
        healthcheck:
            test: [ "CMD", "curl", "-f", "http://localhost:80/api/health" ]
            retries: 3
            timeout: 10s

```

#### faq:
how can generate APP_KEY?
```bash
cat /dev/urandom | head -c 32 | base64
```
