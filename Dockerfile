FROM ubuntu:21.04

WORKDIR /var/www/html

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Asia/Tehran

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get install -y gnupg gosu ca-certificates zip unzip supervisor curl libcap2-bin \
    && mkdir -p ~/.gnupg \
    && chmod 600 ~/.gnupg \
    && echo "disable-ipv6" >> ~/.gnupg/dirmngr.conf \
    && apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys E5267A6C \
    && apt-key adv --homedir ~/.gnupg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C300EE8C \
    && echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu hirsute main" > /etc/apt/sources.list.d/ppa_ondrej_php.list

RUN apt-get update \
    && apt-get install -y php8.1-cli \
       php8.1-pgsql \
       php8.1-curl \
       php8.1-mbstring \
       php8.1-xml php8.1-bcmath \
       php8.1-intl php8.1-readline \
       php8.1-msgpack php8.1-redis \
    && php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer \
    && apt-get update \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN setcap "cap_net_bind_service=+ep" /usr/bin/php8.1

COPY .docker/production/start-container /usr/local/bin/start-container
COPY .docker/production/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY .docker/production/php.ini /etc/php/8.1/cli/conf.d/99-sail.ini
RUN chmod +x /usr/local/bin/start-container

COPY composer.json ./
COPY composer.lock ./

RUN COMPOSER_MEMORY_LIMIT=-1 composer install --no-dev --optimize-autoloader --no-scripts

COPY . ./

RUN composer run-script post-autoload-dump && composer run-script post-update-cmd

RUN ./vendor/bin/rr get-binary --filter=2.7.9

RUN chmod +x ./rr

EXPOSE 80

ENTRYPOINT ["start-container"]
