<?php

use App\Http\Controllers\HealthController;
use Illuminate\Support\Facades\Route;


Route::get('/health', [HealthController::class, 'index'])->name('health');
