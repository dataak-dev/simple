<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HealthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_health_check_route_is_available()
    {
        $response = $this->get('/health');

        $response->assertStatus(200);
    }
}
